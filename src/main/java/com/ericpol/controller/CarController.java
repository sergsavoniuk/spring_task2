package com.ericpol.controller;

import com.ericpol.dto.CarTo;
import com.ericpol.exception.CarNotFound;
import com.ericpol.model.Car;
import com.ericpol.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CarController {

    @Autowired
    private CarService carService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String car(Model model) {
        return "redirect:/list-cars";
    }

    @ResponseBody
    @RequestMapping(value = "/save-car", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Car saveCar(@RequestBody @Valid CarTo carTo) {
        return carService.saveCar(carTo);
    }

    @ResponseBody
    @RequestMapping(value = "/edit-car", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Car editCar(@RequestBody @Valid Car car) throws CarNotFound {
        return carService.updateCar(car);
    }

    @ResponseBody
    @RequestMapping(value = "/delete-car", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Car deleteCar(@RequestBody String id) throws CarNotFound {
        return carService.deleteCar(id.substring(1, id.length() - 1));
    }

    @ResponseBody
    @RequestMapping(value = "/send-cars", method = RequestMethod.GET, produces = "application/json")
    public List<Car> getAllCars() {
        return carService.findAll();
    }

    @RequestMapping(value = "/list-cars", method = RequestMethod.GET)
    public String listOfCars(ModelMap modelMap) {
        return "index";
    }
}
