package com.ericpol.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

public class CarTo {

    @NotEmpty
    private String makes;
    @NotEmpty
    private String model;
    @NotEmpty
    private String fuel;
    @Range(min = 1980, max = 2017)
    private Integer year;
    @Range(min = 0, max = 100000)
    private Integer price;

    public CarTo() {
    }

    public CarTo(String makes, String model, String fuel, Integer year, Integer price) {
        this.makes = makes;
        this.model = model;
        this.fuel = fuel;
        this.year = year;
        this.price = price;
    }

    public String getMakes() {
        return makes;
    }

    public void setMakes(String makes) {
        this.makes = makes;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "CarTo{" +
                "makes='" + makes + '\'' +
                ", model='" + model + '\'' +
                ", fuel='" + fuel + '\'' +
                ", year=" + year +
                ", price=" + price +
                '}';
    }
}