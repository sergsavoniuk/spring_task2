package com.ericpol.dto;

import com.ericpol.model.Car;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class FactoryTo {

    public Car getCar(CarTo carTo) {
        Car car = new Car();
        car.setId(UUID.randomUUID().toString());
        car.setMakes(carTo.getMakes());
        car.setModel(carTo.getModel());
        car.setFuel(carTo.getModel());
        car.setYear(carTo.getYear());
        car.setPrice(carTo.getPrice());
        return car;
    }
}