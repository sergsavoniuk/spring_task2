package com.ericpol.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @Column(name = "id", nullable = false)
 /*   @GeneratedValue(strategy = GenerationType.IDENTITY)*/
    private String id;

    @NotEmpty
    @Column(name = "makes", nullable = false)
    private String makes;

    @NotEmpty
    @Column(name = "model", nullable = false)
    private String model;

    @NotEmpty
    @Column(name = "fuel", nullable = false)
    private String fuel;

    @Range(min = 1980, max = 2017)
    @Column(name = "year", nullable = false)
    private Integer year;

    @Range(min = 0, max = 100000)
    @Column(name = "price", nullable = false)
    private Integer price;

    public Car() {

    }

    public Car(String id, String makes, String model, String fuel, Integer year, Integer price) {
        this.id = id;
        this.makes = makes;
        this.model = model;
        this.fuel = fuel;
        this.year = year;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMakes() {
        return makes;
    }

    public void setMakes(String makes) {
        this.makes = makes;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "makes='" + makes + '\'' +
                ", model='" + model + '\'' +
                ", fuel='" + fuel + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", id=" + id +
                '}';
    }

}