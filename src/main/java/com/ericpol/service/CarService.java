package com.ericpol.service;

import com.ericpol.dto.CarTo;
import com.ericpol.exception.CarNotFound;
import com.ericpol.model.Car;

import java.util.List;

public interface CarService {
    Car saveCar(CarTo carTo);
    Car deleteCar(String id) throws CarNotFound;
    Car updateCar(Car car) throws CarNotFound;
    Car findById(String id);
    List<Car> findAll();
}
