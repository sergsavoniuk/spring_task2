package com.ericpol.service;

import com.ericpol.dao.CarDao;
import com.ericpol.dto.CarTo;
import com.ericpol.dto.FactoryTo;
import com.ericpol.exception.CarNotFound;
import com.ericpol.model.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarDao carDao;

    @Autowired
    private FactoryTo factoryTo;

    @Transactional
    @Override
    public Car saveCar(CarTo carTo) {
        Car car = factoryTo.getCar(carTo);
        car = carDao.save(car);
        return car;
    }

    @Transactional
    @Override
    public Car deleteCar(String id) throws CarNotFound {
        Car deletedCar = findById(id);
        if (deletedCar == null) {
            throw new CarNotFound();
        }
        carDao.delete(deletedCar);
        return deletedCar;
    }

    @Transactional
    @Override
    public Car updateCar(Car car) throws CarNotFound {
        Car updatedCar = findById(car.getId());
        if (updatedCar == null) {
            throw new CarNotFound();
        }
        updatedCar.setMakes(car.getMakes());
        updatedCar.setModel(car.getModel());
        updatedCar.setFuel(car.getFuel());
        updatedCar.setYear(car.getYear());
        updatedCar.setPrice(car.getPrice());
        return updatedCar;
    }

    @Transactional(readOnly = true)
    @Override
    public Car findById(String id) {
        return carDao.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Car> findAll() {
        return (List<Car>) carDao.findAll();
    }
}