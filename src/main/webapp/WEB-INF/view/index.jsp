<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Spring MVC</title>
    <spring:url value="/resources/css/bootstrap.min.css"
                var="bootstrapCss" />
    <link href="${bootstrapCss}" rel="stylesheet" />
    <spring:url value="/resources/js/jquery.1.10.2.min.js"
                var="jqueryJs" />
    <script src="${jqueryJs}"></script>

    <script type="text/javascript" src="/resources/js/functional.js"></script>
    <script type="text/javascript" src="/resources/js/validation.js"></script>
</head>
<body>
<div class="container">
    <h2 align="center">Add/Edit car</h2>
    <form class="form-horizontal">
        <div class="form-group">
            <div>
                <input type=text id="id" hidden="hidden">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Makes</label>
            <div class="col-sm-7">
                <input type=text class="form-control" id="makes">
            </div>
            <div>
                <span style="color:red" id="makesErr"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Model</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="model">
            </div>
            <div>
                <span style="color:red" id="modelErr"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Fuel</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="fuel">
            </div>
            <div>
                <span style="color:red" id="fuelErr"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Year</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="year">
            </div>
            <div>
                <span style="color:red" id="yearErr"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Price</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="price">
            </div>
            <div>
                <span style="color:red" id="priceErr"></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-3">
                <button type="button" id="btn-add"
                        class="btn btn-primary">Add</button>
                <button type="button" id="btn-save"
                        class="btn btn-primary">Save</button>
                <button type="button" id="btn-cancel"
                        class="btn btn-primary">Cancel</button>
            </div>
        </div>
    </form>
</div>

<div class="container">
    <h2 align="center">List of cars</h2>
    <table class="table table-bordered" id="cars-table">
        <thead>
        <tr class="text-center">
            <th class="text-center">ID</th>
            <th class="text-center">Makes</th>
            <th class="text-center">Model</th>
            <th class="text-center">Fuel</th>
            <th class="text-center">Year</th>
            <th class="text-center">Price</th>
            <th class="text-center" colspan="2">Action</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script>
    $('#btn-add').click(function() {
        if (validate()) {
            addCar();
        }
    });

    $('#btn-cancel').click(function() {
        clearInputs();
        enabledAddButton();
    });

    window.onload = function() {
        enabledAddButton();
        showAllCars();
    };
</script>

</body>
</html>