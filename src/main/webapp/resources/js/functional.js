cars = [];
var editRowIndex = 0;

function showAllCars() {
    $.ajax( {
        type: 'GET',
        contentType: 'application/json',
        url: '/send-cars',
        success: function(data) {
            for (var i = 0; i < data.length; ++i) {
                addRow('cars-table', data[i]);
                cars[i] = data[i];
            }
        }
    });
}

function addCar() {
    var makes = $('#makes').val();
    var model = $('#model').val();
    var fuel = $('#fuel').val();
    var year = $('#year').val();
    var price = $('#price').val();

    $.ajax( {
        type: 'POST',
        contentType: 'application/json',
        url: '/save-car',
        data: JSON.stringify({'makes':makes, 'model':model, 'fuel':fuel, 'year':year, 'price':price}),
        dataType: 'json',
        success: function(data) {
            addRow('cars-table', data);
            clearInputs();
            clearSpan();
            cars.push(data);
        },
        error: function(error) {
            showErrors(error);
        }
    });
}

function showErrors(error) {
    clearSpan();
    for (var i = 0; i < error.responseJSON.errors.length; ++i) {
        var arr = error.responseJSON.errors[i].split(',');
        var field = arr[0].trim();
        var message = arr[1].trim();
        addErrorMessageToField(field, message);
    }
}

function addErrorMessageToField(field, message) {
    switch (field) {
        case 'makes':
            $('#makesErr').text('*' + message);
            break;
        case 'model':
            $('#modelErr').text('*' + message);
            break;
        case 'fuel':
            $('#fuelErr').text('*' + message);
            break;
        case 'year':
            $('#yearErr').text('*' + message);
            break;
        case 'price':
            $('#priceErr').text('*' + message);
            break;
        default:
            alert(message);
            break;
    }
}

function addCol(row, cellNum, value) {
    if (typeof value === 'string' || typeof value === 'number') {
        row.insertCell(cellNum).innerHTML = value;
    } else {
        row.insertCell(cellNum).appendChild(value);
    }
}

function addRow(tableId, data) {
    var table = document.getElementById(tableId);
    var row = table.insertRow(-1);
    row.className = "text-center";

    addCol(row, 0, data.id);
    addCol(row, 1, data.makes);
    addCol(row, 2, data.model);
    addCol(row, 3, data.fuel);
    addCol(row, 4, data.year);
    addCol(row, 5, data.price);

    var buttonEdit = document.createElement('button');
    buttonEdit.className = 'btn btn-primary btn-sm';
    buttonEdit.innerHTML = 'Edit';

    addCol(row, 6, buttonEdit);

    buttonEdit.addEventListener("click", function() {
        editRowIndex = getEditedRowIndex(this);
        setEditedData(editRowIndex);
        disableAddButton();
    });

    var buttonDelete = document.createElement('button');
    buttonDelete.className = 'btn btn-danger btn-sm';
    buttonDelete.innerHTML = 'Delete';

    addCol(row, 7, buttonDelete);

    buttonDelete.addEventListener("click", function() {
        var index = getDeletedRowIndex(this);

        $.ajax( {
            type: 'POST',
            contentType: 'application/json',
            url: '/delete-car',
            data: JSON.stringify(cars[index - 1].id),
            dataType: 'json',
            success: function(data) {
                deleteRow(tableId, index);
                cars.splice(index - 1, 1);
            }
        });
    });

    $('#btn-save').click(function() {
        if (validate()) {
            var json = getNewData();

            $.ajax( {
                type: 'POST',
                contentType: 'application/json',
                url: '/edit-car',
                data: json,
                dataType: 'json',
                success: function(data) {
                    cars.splice(editRowIndex - 1, 1);
                    cars[editRowIndex - 1] = data;
                    editRow(tableId, editRowIndex);
                    clearInputs();
                    clearSpan();
                    enabledAddButton();
                },
                error: function(error) {
                    showErrors(error);
                }
            });
        }
    });

/*    $('#btn-cancel').click(function() {
        clearInputs();
        enabledAddButton();
    });*/
}

function getDeletedRowIndex($this) {
    return $this.parentNode.parentNode.rowIndex;
}

function getEditedRowIndex($this) {
    editRowIndex = $this.parentNode.parentNode.rowIndex;
    return editRowIndex;
}

function deleteRow(tableId, index) {
    document.getElementById(tableId).deleteRow(index);
}

function setEditedData(index) {
    $('#id').val(cars[index - 1].id);
    $('#makes').val(cars[index - 1].makes);
    $('#model').val(cars[index - 1].model);
    $('#fuel').val(cars[index - 1].fuel);
    $('#year').val(cars[index - 1].year);
    $('#price').val(cars[index - 1].price);
}

function disableAddButton() {
    $('#btn-add').attr('disabled',true);
    $('#btn-save').removeAttr('disabled');
    $('#btn-cancel').removeAttr('disabled');
}

function enabledAddButton() {
    $('#btn-add').removeAttr('disabled');
    $('#btn-save').attr('disabled', true);
    $('#btn-cancel').attr('disabled', true);
}

function getNewData() {
    var id = $('#id').val();
    var makes = $('#makes').val();
    var model = $('#model').val();
    var fuel = $('#fuel').val();
    var year = $('#year').val();
    var price = $('#price').val();
    return JSON.stringify({'id': id, 'makes': makes, 'model': model, 'fuel': fuel, 'year': year, 'price': price});
}

function editRow(tableId, index) {
    var table = document.getElementById(tableId);
    var newRow = table.insertRow(index);
    newRow.className = "text-center";
    editCol(newRow, 0, cars[index - 1].id);
    editCol(newRow, 1, cars[index - 1].makes);
    editCol(newRow, 2, cars[index - 1].model);
    editCol(newRow, 3, cars[index - 1].fuel);
    editCol(newRow, 4, cars[index - 1].year);
    editCol(newRow, 5, cars[index - 1].price);

    var buttonEdit = document.createElement('button');
    buttonEdit.className = 'btn btn-primary btn-sm';
    buttonEdit.innerHTML = 'Edit';

    editCol(newRow, 6, buttonEdit);

    buttonEdit.addEventListener("click", function() {
        editRowIndex = getEditedRowIndex(this);
        setEditedData(editRowIndex);
        disableAddButton();
    });

    var buttonDelete = document.createElement('button');
    buttonDelete.className = 'btn btn-danger btn-sm';
    buttonDelete.innerHTML = 'Delete';

    editCol(newRow, 7, buttonDelete);

    buttonDelete.addEventListener("click", function() {
        var index = getDeletedRowIndex(this);

        $.ajax( {
            type: 'POST',
            contentType: 'application/json',
            url: '/delete-car',
            data: JSON.stringify(cars[index - 1].id),
            dataType: 'json',
            success: function(data) {
                deleteRow(tableId, index);
                cars.splice(index - 1, 1);
            }
        });
    });
    table.deleteRow(index + 1);
}

function editCol(row, colIndex, value) {
    if (typeof value === 'string' || typeof value == 'number') {
        row.insertCell(colIndex).innerHTML = value;
    } else {
        row.insertCell(colIndex).appendChild(value);
    }
}

function clearInputs() {
    $('#id').val('');
    $('#makes').val('');
    $('#model').val('');
    $('#fuel').val('');
    $('#year').val('');
    $('#price').val('');
}

function clearSpan() {
    $('#makesErr').text('');
    $('#modelErr').text('');
    $('#fuelErr').text('');
    $('#yearErr').text('');
    $('#priceErr').text('');
}